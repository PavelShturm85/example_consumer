"""Life status dialog message handler."""
import abc
import typing

from queues import external, services
from queues.models import dialogs as dialogs_models
from queues.models import entities
from queues.models import operator as operator_models
from queues.models import websocket as websocket_models
from queues.repositories import queues as queues_repositories


class BaseStatusService:
    """Base status service."""

    def __init__(
        self,
        queues_repository: queues_repositories.QueuesRepository,
        private_users_websocket_client: external.PrivateUserWebsocketClient,
    ):
        self._queues_repository: queues_repositories.QueuesRepository = queues_repository
        self._private_users_websocket_client: external.PrivateUserWebsocketClient = private_users_websocket_client

    def _convert_message_to_dialog(self, message: dialogs_models.DialogsLifeStatusMessage) -> entities.QueueDialog:
        """Prepare message for add."""
        return entities.QueueDialog(
            dialog_id=message.dialog_id,
            dialog_created_datetime=message.time_stamp,
            public_user_id=message.user.user_info.user_id,
            public_user_info=entities.AuthorInfo.construct_from_user(message.user),
        )


class LifeStatusServiceProtocol(typing.Protocol):
    """Life status service protocol."""

    @abc.abstractmethod
    async def handle_status(self, message: dialogs_models.DialogsLifeStatusMessage):
        """Handle status."""
        raise NotImplementedError()


class EndStatusService(BaseStatusService):
    """Handle end status."""

    def __init__(
        self,
        queues_repository: queues_repositories.QueuesRepository,
        private_users_websocket_client: external.PrivateUserWebsocketClient,
        move_dialogs_service: services.DialogsMovingService,
    ):
        super().__init__(queues_repository, private_users_websocket_client)
        self._move_dialogs_service: services.DialogsMovingService = move_dialogs_service

    async def handle_status(self, message: dialogs_models.DialogsLifeStatusMessage) -> None:
        """Handle status."""
        await self._private_users_websocket_client.send_dialog_message(
            await self._queues_repository.fetch_dialog(message.dialog_id),
            event_type=websocket_models.QueueDialogMessageEventType.dialog_closed,
            user=message.user,
        )
        await self._queues_repository.remove_dialog_from_personal_queue(
            message.dialog_id, message.user.user_info.user_id
        )
        operator_status: int = await self._queues_repository.fetch_operator_status(message.user.user_info.user_id)
        dialogs_amount: int = await self._queues_repository.fetch_amount_dialogs_by_operator(
            message.user.user_info.user_id
        )
        operator: operator_models.Operator = operator_models.Operator(
            id=message.user.user_info.user_id, current_status=operator_status, dialogs_amount=dialogs_amount
        )
        if operator.free_slots_count:
            await self._move_dialogs_service.move_dialogs_to_personal_queue(message.user)


class StartStatusService(BaseStatusService):
    """Handle start status."""

    def __init__(
        self,
        queues_repository: queues_repositories.QueuesRepository,
        private_users_websocket_client: external.PrivateUserWebsocketClient,
        dialogs_distribution_service: services.DialogsRandomDistributionService,
    ):
        super().__init__(queues_repository, private_users_websocket_client)
        self._dialogs_distribution_service: services.DialogsRandomDistributionService = dialogs_distribution_service

    async def handle_status(self, message: dialogs_models.DialogsLifeStatusMessage) -> None:
        """Handle status."""
        dialog: entities.QueueDialog = self._convert_message_to_dialog(message)
        await self._dialogs_distribution_service.distribute_one_dialog(dialog, message.user)


class LifeStatusServiceFactory:
    """Life status service factory."""

    def __init__(
        self,
        queues_repository: queues_repositories.QueuesRepository,
        private_users_websocket_client: external.PrivateUserWebsocketClient,
        move_dialogs_service: services.DialogsMovingService,
        dialogs_distribution_service: services.DialogsRandomDistributionService,
    ):
        self.life_status_services_map: typing.Dict = {
            dialogs_models.DialogLifeStatusEnum.start: StartStatusService(
                queues_repository, private_users_websocket_client, dialogs_distribution_service
            ),
            dialogs_models.DialogLifeStatusEnum.end: EndStatusService(
                queues_repository, private_users_websocket_client, move_dialogs_service
            ),
        }

    def create_service(self, life_status: int) -> typing.Optional[LifeStatusServiceProtocol]:
        """Factory life statuses."""
        return self.life_status_services_map.get(life_status, None)
