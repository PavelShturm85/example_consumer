"""Run dialog consumer."""
import asyncio

import sentry_sdk
from chat_log_tools import setup_json_logger

from queues import settings
from queues.dialog_life_status_consumer.consumers import run_life_status_dialog_consumer


if settings.SENTRY_DSN:
    sentry_sdk.init(dsn=settings.SENTRY_DSN)

setup_json_logger(need_sentry=bool(settings.SENTRY_DSN))


def main():
    """Run dialog life status consumer in loop."""
    asyncio.run(run_life_status_dialog_consumer())
