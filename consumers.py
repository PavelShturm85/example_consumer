"""Consumer for fetch message from dialog."""
import asyncio
import pathlib
import typing

import pydantic
from aioredis.errors import ConnectionClosedError, MultiExecError
from chat_log_tools import middleware as log_tools_middleware
from chat_mq_utils import generic_consumer
from kerberos_tools import client as kerberos_client
from loguru import logger
from pydantic.error_wrappers import ValidationError

from queues import connections, external
from queues import services as common_services
from queues import settings
from queues.dialog_life_status_consumer import services
from queues.models import dialogs
from queues.repositories import queues as queues_repositories


class DialogLifeStatusConsumer(generic_consumer.AsyncBaseKafkaConsumer):
    """Consume from dialog life status message."""

    encoder: pydantic.main.ModelMetaclass = dialogs.DialogsLifeStatusMessage
    processing_statuses: tuple = (
        dialogs.DialogLifeStatusEnum.start,
        dialogs.DialogLifeStatusEnum.end,
    )

    def __init__(
        self,
        service_factory: services.LifeStatusServiceFactory,
        middleware: typing.Optional[typing.List[typing.Callable]] = None,
    ) -> None:
        super().__init__(middleware)
        self._service_factory: services.LifeStatusServiceFactory = service_factory

    async def process_one_message(self, message: dialogs.DialogsLifeStatusMessage) -> None:
        """Does the main task."""
        if message.status in self.processing_statuses:
            life_status_service: typing.Optional[
                services.LifeStatusServiceProtocol
            ] = self._service_factory.create_service(message.status)
            if life_status_service:
                try:
                    await life_status_service.handle_status(message)
                except (ValidationError, MultiExecError, ConnectionClosedError):
                    logger.exception(f'Cannot process message: {message}')
            else:
                logger.error(f'There is no service for life_status: {message.status}')

        await self.consumer.commit()


async def run_life_status_dialog_consumer() -> None:
    """Run life status dialog consumer."""
    kerberos_renewal_task: typing.Optional[asyncio.Task] = None
    if settings.KAFKA_USE_KERBEROS:
        kerberos_renewal_task = await kerberos_client.issue_ticket_and_run_renewal_task(
            settings.KAFKA_KERBEROS_PRINCIPAL,
            pathlib.Path(settings.KAFKA_KERBEROS_KEYTAB_PATH),
            settings.KAFKA_KERBEROS_RENEWAL_INTERVAL_SECONDS,
        )
    await connections.RedisConnection.initialize()
    await connections.KafkaProducerConnection.initialize()
    queues_repository: queues_repositories.QueuesRepository = queues_repositories.QueuesRepository(
        connections.RedisConnection()
    )
    private_user_websocket_client: external.PrivateUserWebsocketClient = external.PrivateUserWebsocketClient(
        connections.KafkaProducerConnection.get_connection()
    )
    dialog_life_status_message_client: external.DialogLifeStatusMessageClient = external.DialogLifeStatusMessageClient(
        connections.KafkaProducerConnection.get_connection()
    )
    service_factory: services.LifeStatusServiceFactory = services.LifeStatusServiceFactory(
        queues_repository,
        private_user_websocket_client,
        common_services.DialogsMovingService(
            queues_repository, private_user_websocket_client, dialog_life_status_message_client
        ),
        common_services.DialogsRandomDistributionService(
            queues_repository, private_user_websocket_client, dialog_life_status_message_client
        ),
    )
    consumer: DialogLifeStatusConsumer = DialogLifeStatusConsumer(
        service_factory, middleware=[log_tools_middleware.RequestIDConsumerMiddleware()]
    ).config_driver(
        settings.KAFKA_TOPIC_LIFE_DIALOG_STATUS,
        group_id=settings.KAFKA_LIFE_DIALOG_STATUS_CONSUMER_GROUP,
        enable_auto_commit=False,
        bootstrap_servers=settings.KAFKA_BOOTSTRAP_SERVERS,
        security_protocol=settings.KAFKA_SECURITY_PROTOCOL,
        sasl_mechanism=settings.KAFKA_SASL_MECHANISM,
        sasl_kerberos_service_name=settings.KAFKA_KERBEROS_SERVICE_NAME,
    )
    try:
        await consumer.start()
    finally:
        await connections.RedisConnection.shutdown()
        await connections.KafkaProducerConnection.shutdown()
        if kerberos_renewal_task:
            kerberos_renewal_task.cancel()
